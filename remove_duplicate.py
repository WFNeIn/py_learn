def removeDuplicate(nums:list) -> int:
    n = len(nums)

    if n == 0:
        return 0

    slow, fast = 0, 1
    while fast < n:
        if nums[fast] != nums[slow]:
            slow += 1
            nums[slow] = nums[fast]
        fast += 1
    return slow + 1

if __name__ == '__main__':
    l = [1,3,5,6,9,0,1,3,4,6,7]
    l.sort()

    clip = len(l) - removeDuplicate(l)
    while clip > 0:
        l.pop(len(l)-1)
        clip -= 1
    print(l)
