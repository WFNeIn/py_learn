
def trap(height:list) -> int:
    if not height:
        return 0
    n = len(height)
    left, right = 0, n-1
    ans = 0

    l_max = height[0]
    r_max = height[n - 1]

    while left <= right:
        l_max = max(l_max,height[left])
        r_max = max(r_max,height[right])

        if l_max < r_max:
            ans += l_max - height[left]
            left += 1
        else:
            ans += r_max - height[right]
            right -= 1
    return ans

if __name__ == '__main__':
    # h = [1,2,3,7,9,3,4,6,7,8,2]
    h = [1, 2]
    print(trap(h))
    len(h)

