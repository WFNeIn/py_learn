import sys

class Node:
    def __init__(self,data):
        self.data = data
        self.left = None
        self.right = None
        self.parent = None

    def __repr__(self):
        return str(self.data)

class Bst():
    def __init__(self,root= None):
        self.root = root

    def insert(self,data):
        if data is None:
            raise TypeError("data cannot be None")
        if self.root is None:
            self.root = Node(data)
            return self.root
        else:
            return None

    def _insert(self,node,data):
        if node is None:
            return Node(data)
        if data <= node.data:
            if node.left is None:
                node.left = self._insert(node.left,data)
                node.left.parent = node
                return node.left
            else:
                return self._insert(node.left,data)
        else:
            if node.right is None:
                node.right = self._insert(node.right,data)
                node.right.parent = node
                return node.right
            else:
                return self._insert(node.right,data)

    def height(self):
        if self.root is None:
            return 0
        return 1 + max(self.height(self.root.left),self.height(self.root.right))

    def check_balance(self):
        pass

    def _check_balance(self,node):
        if node is None:
            return  0
        left_height = self._check_balance(node.left)
        if left_height == -1:
            return -1
        right_height = self._check_balance(node.right)
        if right_height == -1:
            return -1
        diff = abs(left_height- right_height)
        if diff > 1:
            return -1
        return 1 + max(left_height,right_height)


class BstDfs(Bst):
    def in_order_traversal(self,node,visit_func):
        if node is not None:
            self.in_order_traversal(node.left, visit_func)
            visit_func(node)
            self.in_order_traversal(node.right, visit_func)

    def pre_order_traversal(self,node,visit_func):
        visit_func(node)
        self.in_order_traversal(node.left,visit_func)
        self.in_order_traversal(node.right,visit_func)

    def post_order_traversal(self,node,visit_func):
        self.in_order_traversal(node.left,visit_func)
        self.in_order_traversal(node.right,visit_func)
        visit_func(node)

class BstBfs(Bst):
    def bfs(self,visit_func):
        if self.root is None:
            raise TypeError("root is None")
        from collections import deque
        queue = deque()
        queue.append(self.root)
        while queue:
            node = queue.popleft()
            visit_func(node)
            if node.left is not None:
                queue.append(node.left)
            if node.right is not None:
                queue.append(node.right)

class MinBst:
    def create_min_bst(self,array):
        if array is None:
            return
        return self

    def _create_min_bst(self,array,start,end):
        if end < start:
            return None
        mid = (start + end)//2
        node = Node(array[mid])
        node.left = self._create_min_bst(array,start,mid-1)
        node.right = self._create_min_bst(array,mid+1,end)
        return node

class BstValidate(Bst):
    def validate(self):
        if self.root is None:
            raise TypeError("No root node")
        return self._validate(self.root)

    def _validate(self,node,minimum=-sys.maxsize,maximum=sys.maxsize):
        if node is None:
            return True
        if node.data <= minimum or node.data >= maximum:
            return False
        if not self._validate(node.left,minimum,node.data):
            return False
        if not self._validate(node.right,node.data,maximum):
            return False
        return True

class Solution(Bst):
    def find_second_largest(self):
        if self.root is None:
            raise TypeError("root cannot be None")
        if self.root.right is None and self.root.left is None:
            raise ValueError("root must have at least one child")
        return

    def _find_right_most_node(self,node):
        if node.right is not None:
            return self._find_right_most_node(node.right)
        else:
            return node

    def find_second_largest(self):
        if self.root is None:
            raise TypeError("root cannot be None")
        if self.root.right is None and self.root.left is None:
            raise ValueError("root must have at least one child")
        return self._find_right_most_node(self.root)

class BinaryTree():
    def lca(self,root,node1,node2):
        if None in (root,node1,node2):
            return None

    def _node_in_tree(self,root,node):
        if root is None:
            return False
        if root is node:
            return True
        left = self._node_in_tree(root.left,node)
        right = self._node_in_tree(root.right,node)
        return left or right

    def _lca(self,root,node1,node2):
        if root is None:
            return None
        if root is node1 or root is node2:
            return root
        left_node = self._lca(root.left,node)
        right_node = self._lca(root.right,node1,node2)
        if left_node is not None and right_node is not None:
            return root
        else:
            return left_node if left_node is not None else right_node

if __name__ == '__main__':
    print(sys.maxsize)

