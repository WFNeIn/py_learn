from enum import Enum
from collections import deque
from priority_queue import PriorityQueue,PriorityQueueNode
import sys

class State(Enum):
    unvisited = 0
    visiting = 0
    visited = 2

class Node:
    def __init__(self,key):
        self.key = key
        self.visit_state = State.unvisited
        self.incoming_edges = 0
        self.adj_nodes = {}
        self.adj_weights = {}

    def __repr__(self):
        return  str(self.key)

    def __lt__(self, other):
        return self.key < other.key

    def add_neighbor(self,neighbor,weight=0):
        if neighbor is None or weight is None:
            raise TypeError("neighbor or weight cannot be None")
        neighbor.incoming_edges +=1
        self.adj_weights[neighbir.key] = weight
        self.adj_nodes[neighbor.key] = neighbor

    def remove_neighbor(self,neighbor):
        if neighbor is None:
            raise TypeError("neighbor cannot be None")
        if neighbor.key not in self.adj_nodes:
            raise KeyError("neighbior not found")
        neighbor.incoming_edges -= 1
        del self.adj_weights[neighbor.key]
        del self.adj_nodes[neighbor.key]

class Graph:
    def __init__(self):

        self.nodes = {}

    def add_node(self,key):
        if key is None:
            raise TypeError("key cannot be None")
        if key not in self.nodes:
            self.nodes[key] = Node(key)
        return self.nodes[key]

    def add_edge(self,source_key,dest_key,weight=0):
        if source_key is None or dest_key is None:
            raise KeyError("Invalid key")
        if source_key not in self.nodes:
            self.add_node(source_key)
        if dest_key not in self.nodes:
            self.add_node(dest_key)
        self.nodes[source_key].add_neighbor(self.nodes[dest_key],weight)

    def add_undirected_edge(self,src_key,dst_key,weight=0):
        if src_key is None or dst is None:
            raise TypeError("key cannot be None")
        self.add_edge(src_key,dst_key,weight)
        self.add_edge(dst_key,src_key,weight)

class GraphDfs(Graph):
    def dfs(self,root,visit_func):
        if root is None:
            return
        visit_func(root)
        root.visit_state = State.visited
        for node in root.adj_nodes.values():
            if node.visit_state == State.unvisited:
                self.dfs(node,visit_func)

class GraphBfs(Graph):

    def bfs(self,root,visit_func):
        if root is None:
            return
        queue = deque()
        queue.append(root)
        root.visit_state = State.visited
        while queue:
            node = queue.popleft()
            visit_func(node)
            for adjacent_node in node.adj_nodes.values():
                if adjacent_node.visit_state == State.unvisited:
                    queue.append(adjacent_node)
                    adjacent_node.visit_state = State.visited

class GraphPathExists(Graph):

    def path_exists(self,start,end):
        if start is None or end is None:
            return False
        if start is end:
            return True
        queue = deque()
        queue.append(start)
        start.visit_state = State.visited

        while queue:
            node = queue.popleft()
            if node is end:
                return  True
            for adj_node in node.adj_nodes.values():
                if adj_node.visit_state == State.unvisited:
                    queue.append(adj_node)
                    adj_node.visit_state = State.visited
        return False

    def path_exist_from_dfs(self,start,end):
        if start is None or end is None:
            return False
        if start is end:
            return True
        start.visit_state = True
        for node in start.adj_node:
            if node.visit_state == State.unvisited:
                self.path_exist_from_dfs(start,node)

 # class ShortedtPath:
 #     def __init__(self,graph):
 #         if graph is None:
 #             raise TypeError("graph can not be None")
 #         self.graph = graph
 #         self.previous = {}
 #         self.path_weight = {}
 #         self.remaining = PriorityQueue()
 #         for key in self.graph.nodes.keys():
 #             self.previous[key] = None
 #             self.path_weight[key] = sys.maxsize
 #             self.remaining.insert(PriorityQueueNode(key,self.path_weight[key]))
 #
 #     def find_shortest_path(self,start_node_key,end_node_key):
 #         if start_node_key is None or end_node_key is None:
 #             raise TypeError("Input node keys cannot be None")
 #         if (start_node_key not in self.graph.nodes or end_node_key not in self.graph.nodes):
 #             raise ValueError("Invalid start or end node key")
 #         self.path_weight[start_node_key] = 0
 #         self.remaining.decrease_key(start_node_key,0)
 #         while self.remaining:
 #             min_node_key = self.remaining.extract_min().obj()
 #             min_node = self.graph.nodes[min_node_key]
 #             for adj_key in min_node.adj_nodes.keys():
 #                 new_weight = min_node.adj_weights[adj_key] + self.path_weight[min_node_key]
 #                 if self.path_weight[adj_key] > new_weight:
 #                     self.previous[adj_key]



