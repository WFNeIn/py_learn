# Time:O(n)
# Space:Additional O(n)
class UniqueCharSet:
    def has_unique_chars(self,string):
        if string is None:
            return False
        return len(set(string)) == len(string)


# Time:O(n)
# Space:Additional O(n)
class UniqueChars:
    def has_unique_chars(self, string):
        if string is None:
            return False
        chars_set = set()
        for char in string:
            if char in chars_set:
                return False
            else:
                chars_set.add(char)
        return True



