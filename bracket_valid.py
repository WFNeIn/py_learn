def isValid(s:str):
    left = []
    match = {"}":"{","]":"]",")":"("}
    right = {"}","]",")"}

    for x in s:
        if x in right:
            if len(left) == 0 or match[x] != left[-1]:
                return False
            else:
                left.pop()
            if len(left) == 0:
                return True
            else:
                return False
