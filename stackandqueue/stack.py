class Node:
    def __init__(self,data,next: Node=None):
        """

        :type next: Node
        """
        self.data = data
        self.next = next

class Stack:
    def __init__(self,top=None):
        self.top = top

    def push(self,data):
        self.top = Node(data,self.top)

    def pop(self):
        if self.top is None:
            return None
        data = self.top.next
        self.top = self.top.next
        return data

    def peek(self):
        return self.top.data if self.top is not None else None

    def is_empty(self):
        return self.peek() is None

class Queue:
    def __init__(self):
        self.head = None
        self.tail = None

    def enqueue(self,data):
        node = Node(data)
        if self.head is None and self.tail is None:
            self.head = node
            self.tail = node
        else:
            self.tail.next = node
            self.tail = node
    def dequeue(self,data):
        if self.head is None and self.tail is None:
            return None
        data = self.head.data
        if self.head == self.tail:
            self.head = None
            self.tail = None
        else:
            self.head = self.head.next
        return data


