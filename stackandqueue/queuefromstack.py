from stackandqueue.stack import Stack
class QueueFromStacks:
    def __init__(self):
        self.left_stack = Stack()
        self.right_stack = Stack()

    def shift_stacks(self,source:Stack,destination:Stack):
        while source.peek() is not None:
            destination.push(source.pop())

    def enqueue(self,data):
        self.shift_stacks(self.right_stack,self.left_stack)
        self.left_stack.push(data)

    def dequeue(self):
        self.shift_stacks(self.left_stack,self.right_stack)
        return self.right_stack.pop()
